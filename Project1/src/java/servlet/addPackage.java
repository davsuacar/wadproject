/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DAVID
 */
@WebServlet(name = "addPackage", urlPatterns = {"/addPackage"})
public class addPackage extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        //PUT ALL THE VARIABLES EXTRACTED FROM THE FORM
        
        HttpSession session=request.getSession();
        String sessionName=(String) session.getAttribute("userName");
        
        Integer aux = Integer.parseInt(request.getParameter("requestPack"));
       
                if(sessionName==null){
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>    <head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
"        <link href=\"default.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
"    <div id=\"header\">\n" +
"	<div id=\"logo\">\n" +
"		<h1><a href=\"#\"><span>GYM</span>PUB</a></h1>\n" +
"                <p>Designed by Students for Students</p> \n" +
"	</div>\n" +
"	<div id=\"menu\">\n" +
"		<ul id=\"main\">\n" +
"                    <li class=\"current_page_item\"><a href=\"index.jsp\">Homepage</a></li>\n" +
"                        <li><a href=\"products.jsp\">Subscription</a></li>\n" +
"			<li><a href=\"packageList.jsp\">Packages</a></li>\n" +
"			<li><a href=\"aboutus.jsp\">About Us</a></li>\n" +
"			<li><a href=\"contactus.jsp\">Contact Us</a></li>\n" +
"		</ul>\n" +
"		<ul id=\"feed\">\n" +
"			<li><a href=\"register.jsp\">Register</a></li>\n" +
"			<li><a href=\"login.jsp\">Login</a></li>\n" +
"		</ul>\n" +
"	</div>\n" +
"	\n" +
"</div>\n" +
"        <!-- end header -->\n" +
"    </head>");
            out.println("<body>");
            out.println("<div id=aboutus>");
            out.println("<h1>Sorry! You have to be logged</h1>");
            out.println("<a href=\"login.jsp\">Login</a>");
            out.println("</id>");
            out.println("</body>");
            out.println("</html>");
            
        }else{
        
        try {
            
            //CONNECTION WITH THE DATABASE
            
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/wadProject","wad","wad");  
            
            
            //EXTRACT THE ID OF THE USER
            String query1 = "SELECT * FROM APP.USERS WHERE EMAIL=?";
            PreparedStatement instr1; 
            instr1 = (PreparedStatement) (Statement) con.prepareStatement(query1);
            instr1.setString(1,sessionName); 
            

            ResultSet rs =instr1.executeQuery();
            
            rs.next();
            Integer id = rs.getInt(1);
            Integer idsubscription = rs.getInt("subscription");
            
            //EXTRACT THE INFORMATION ABOUT THE SUBSCRIPTION
            String query2 = "SELECT PERIOD2 FROM APP.SUBSCRIPTIONS WHERE ID=?";
            PreparedStatement instr2; 
            instr2 = (PreparedStatement) (Statement) con.prepareStatement(query2);
            instr2.setInt(1,idsubscription); 

            ResultSet res2 = instr2.executeQuery();
            
            Integer days = 0;
            
            if(res2.next()){
            java.sql.Date deadline = res2.getDate("PERIOD2");
            
            java.util.Date now = new java.util.Date(System.currentTimeMillis());
            java.util.Date deadlineJava = new java.util.Date(deadline.getTime());
            days = diasTranscurridosEntre(now, deadlineJava);
            }

            if(days <= 0){
                        out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
"        <link href=\"default.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
"    <div id=\"header\">\n" +
"	<div id=\"logo\">\n" +
"		<h1><a href=\"#\"><span>GYM</span>PUB</a></h1>\n" +
"                <p>Designed by Students for Students</p> \n" +
"	</div>\n" +
"	<div id=\"menu\">\n" +
"		<ul id=\"main\">\n" +
"                    <li class=\"current_page_item\"><a href=\"index.jsp\">Homepage</a></li>\n" +
"                        <li><a href=\"products.jsp\">Subscription</a></li>\n" +
"			<li><a href=\"packageList.jsp\">Packages</a></li>\n" +
"			<li><a href=\"aboutus.jsp\">About Us</a></li>\n" +
"			<li><a href=\"contactus.jsp\">Contact Us</a></li>\n" +
"		</ul>\n" +
"		<ul id=\"feed\">\n" +
"			<li><a href=\"register.jsp\">Register</a></li>\n" +
"			<li><a href=\"login.jsp\">Login</a></li>\n" +
"		</ul>\n" +
"	</div>\n" +
"	\n" +
"</div>\n" +
"        <!-- end header -->\n" +
"    </head>");
            out.println("<body>");
            out.println("<div id=aboutus>");
            out.println("<h1>Sorry! You have to be subscripted</h1>");
            out.println("<a href=\"products.jsp\">Subscriptions</a>");
            out.println("</id>");
            out.println("</body>");
            out.println("</html>");
            }
            else{
            //INSERT THE PACKAGES LINKED TO THE USER

            String query3 = "INSERT INTO APP.USERSPACKAGES (USERID,PACKAGEID) VALUES(?,?)";
            PreparedStatement instr3; 
            instr3 = (PreparedStatement) (Statement) con.prepareStatement(query3);
            instr3.setInt(1,id); 
            instr3.setInt(2,aux); 
            instr3.executeUpdate();
            
            
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>    <head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
"        <link href=\"default.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
"    <div id=\"header\">\n" +
"	<div id=\"logo\">\n" +
"		<h1><a href=\"#\"><span>GYM</span>PUB</a></h1>\n" +
"                <p>Designed by Students for Students</p> \n" +
"	</div>\n" +
"	<div id=\"menu\">\n" +
"		<ul id=\"main\">\n" +
"                    <li class=\"current_page_item\"><a href=\"index.jsp\">Homepage</a></li>\n" +
"                        <li><a href=\"products.jsp\">Subscription</a></li>\n" +
"			<li><a href=\"packageList.jsp\">Packages</a></li>\n" +
"			<li><a href=\"aboutus.jsp\">About Us</a></li>\n" +
"			<li><a href=\"contactus.jsp\">Contact Us</a></li>\n" +
"		</ul>\n" +
"		<ul id=\"feed\">\n" +
"			<li><a href=\"register.jsp\">Register</a></li>\n" +
"			<li><a href=\"login.jsp\">Login</a></li>\n" +
"		</ul>\n" +
"	</div>\n" +
"	\n" +
"</div>\n" +
"        <!-- end header -->\n" +
"    </head>");
            out.println("<body>");
            out.println("<div id=aboutus>");
            out.println("<h1>You have been joined to Package number"+aux+"</h1>");
            out.println("<a href=\"packageList.jsp\">Choose other package!</a>");
            out.println("<a href=\"index.jsp\">Home</a>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
            }
        } finally {            
            out.close();
        }
    }
    }
    
       public static int diasTranscurridosEntre(java.util.Date fechaDesde, java.util.Date fechaHasta) {
     long diff = fechaHasta.getTime() - fechaDesde.getTime();
     long dias = diff / (1000 * 60 * 60 * 24);
     return (int) dias;
}

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(addPackage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(addPackage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
