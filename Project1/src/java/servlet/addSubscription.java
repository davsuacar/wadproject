/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DAVID
 */
@WebServlet(name = "addSubscription", urlPatterns = {"/addSubscription"})
public class addSubscription extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        //PUT ALL THE VARIABLES EXTRACTED FROM THE FORM
        
        HttpSession session=request.getSession();
        String sessionName=(String) session.getAttribute("userName");
        
        Integer aux = Integer.parseInt(request.getParameter("product"));
        
        if(sessionName==null){
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>    <head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
"        <link href=\"default.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
"    <div id=\"header\">\n" +
"	<div id=\"logo\">\n" +
"		<h1><a href=\"#\"><span>GYM</span>PUB</a></h1>\n" +
"                <p>Designed by Students for Students</p> \n" +
"	</div>\n" +
"	<div id=\"menu\">\n" +
"		<ul id=\"main\">\n" +
"                    <li class=\"current_page_item\"><a href=\"index.jsp\">Homepage</a></li>\n" +
"                        <li><a href=\"products.jsp\">Subscription</a></li>\n" +
"			<li><a href=\"packageList.jsp\">Packages</a></li>\n" +
"			<li><a href=\"aboutus.jsp\">About Us</a></li>\n" +
"			<li><a href=\"contactus.jsp\">Contact Us</a></li>\n" +
"		</ul>\n" +
"		<ul id=\"feed\">\n" +
"			<li><a href=\"register.jsp\">Register</a></li>\n" +
"			<li><a href=\"login.jsp\">Login</a></li>\n" +
"		</ul>\n" +
"	</div>\n" +
"	\n" +
"</div>\n" +
"        <!-- end header -->\n" +
"    </head>");
            out.println("<body>");
            out.println("<div id=aboutus>");
            out.println("<h1>Sorry! You have to be logged</h1>");
            out.println("<a href=\"login.jsp\">Login</a>");
            out.println("</id>");
            out.println("</body>");
            out.println("</html>");
            
        }else{

        
        
        
        
        
        
        
        
        
        try {
            
            //DATABASE CONNECTION
            
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/wadProject","wad","wad");  
            
            //INSERT THE SUBSCRIPTION
            
            
            String query = "INSERT INTO APP.SUBSCRIPTIONS (PERIOD,PERIOD2) VALUES(null,?)";
            
            PreparedStatement instr2; 
            instr2 = (PreparedStatement) (Statement) con.prepareStatement(query); 
            
            java.util.Date now = new java.util.Date(System.currentTimeMillis());
            java.util.Date dateAdded = null;
            
            if(aux == 1){
                dateAdded = addMonths(now, 1);
            }
            if(aux == 3){
                dateAdded = addMonths(now, 3);
            }
            if(aux == 6){
                dateAdded = addMonths(now, 6);
            }
            
            
            instr2.setDate(1, new java.sql.Date(dateAdded.getTime()));
            instr2.executeUpdate();
            //instr2.setTimestamp(2, (java.sql.Timestamp) fechaSQL); 

            
            
            //LINK THE SUBSCRIPTION TO THE USER
            
                    //EXTRACT THE ID OF THE SUBSCRIPTION
            Statement st = con.createStatement();
            String query2 = "select * from APP.SUBSCRIPTIONS ORDER BY ID DESC";
            ResultSet rs =st.executeQuery(query2);
            Integer counter = null;
            rs.next();
            
            counter = rs.getInt(1);
                
            
            
           
            
            
                    //CHANGE THE SUBSCRIPTION ID OF THE USER
            String query3 = "UPDATE APP.USERS SET SUBSCRIPTION=? WHERE EMAIL=?";
            PreparedStatement instr3; 
            instr3 = (PreparedStatement) (Statement) con.prepareStatement(query3);
            instr3.setInt(1,counter); 
            instr3.setString(2,sessionName); 
            instr3.execute();
            
            
            
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
"        <link href=\"default.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
"    <div id=\"header\">\n" +
"	<div id=\"logo\">\n" +
"		<h1><a href=\"#\"><span>GYM</span>PUB</a></h1>\n" +
"                <p>Designed by Students for Students</p> \n" +
"	</div>\n" +
"	<div id=\"menu\">\n" +
"		<ul id=\"main\">\n" +
"                    <li class=\"current_page_item\"><a href=\"index.jsp\">Homepage</a></li>\n" +
"                        <li><a href=\"products.jsp\">Subscription</a></li>\n" +
"			<li><a href=\"packageList.jsp\">Packages</a></li>\n" +
"			<li><a href=\"aboutus.jsp\">About Us</a></li>\n" +
"			<li><a href=\"contactus.jsp\">Contact Us</a></li>\n" +
"		</ul>\n" +
"		<ul id=\"feed\">\n" +
"			<li><a href=\"register.jsp\">Register</a></li>\n" +
"			<li><a href=\"login.jsp\">Login</a></li>\n" +
"		</ul>\n" +
"	</div>\n" +
"	\n" +
"</div>\n" +
"        <!-- end header -->\n" +
"    </head>");
            out.println("<body>");
            out.println("<div id=aboutus>");
            out.println("<h1>Your subscription was succes!</h1>");
            out.println("<a href=\"index.jsp\">Home</a>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
        
        }
    }
    
    private static java.util.Date addMonths(java.util.Date f, int months) {
      Calendar c = Calendar.getInstance();
      c.setTime(f);
      c.add(Calendar.MONTH, months);  
      return c.getTime();
 } 
    

        


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(addSubscription.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(addSubscription.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
