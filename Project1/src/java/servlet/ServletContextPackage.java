package servlet;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author DAVID
 */
public class ServletContextPackage implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        //String PackageFile = context.getInitParameter("packageList");
        ArrayList packageList = new ArrayList();
        context.setAttribute("packageList", packageList);
        System.out.println("Lista inicializada");

       
        try {
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/wadProject","wad","wad");  
            Statement st = con.createStatement();
            String query = "select * FROM APP.PACKAGES";
            ResultSet rs =st.executeQuery(query);
            while(rs.next())
            {
                //int id =rs.getInt("id");
                String name =rs.getString("name");
                String description =rs.getString("description");
                 model.Package item = new model.Package(name, description);
                 packageList.add(item);
                
            }
             
             } catch (SQLException ex) {
            Logger.getLogger(ServletContextPackage.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }

    public void contextDestroyed(ServletContextEvent sce) {
      
    }
}

