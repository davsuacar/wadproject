/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DAVID
 */
@WebServlet(name = "status", urlPatterns = {"/status"})
public class status extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        HttpSession session=request.getSession();
        String sessionName=(String) session.getAttribute("userName");
        
        
        
        try {
             //DATABASE CONNECTION
            
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/wadProject","wad","wad");  
            
            
            //EXTRACT THE ID OF THE SUBSCRITION and THE USERID
            Statement st = con.createStatement();
            String query1 = "select * from APP.USERS WHERE EMAIL=?";
            PreparedStatement instr1; 
            instr1 = (PreparedStatement) (Statement) con.prepareStatement(query1);
            instr1.setString(1,sessionName); 
            ResultSet rs = instr1.executeQuery();
            
            
            
            Integer idsubscription = null;
            Integer idUser = null;
            rs.next();
            
            idsubscription = rs.getInt("SUBSCRIPTION");
            idUser = rs.getInt("ID");
            
            
            
            //EXTRACT THE ID OF THE PACKAGES
            
            String query2 = "select * from APP.USERSPACKAGES WHERE USERID=?";
            PreparedStatement instr2; 
            instr2 = (PreparedStatement) (Statement) con.prepareStatement(query2);
            instr2.setInt(1,idUser); 
            ResultSet rs2 = instr2.executeQuery();
            
            ArrayList<Integer> listIdPackages = new ArrayList();
            while(rs2.next()){
                Integer idPackage = rs2.getInt("PACKAGEID");
                listIdPackages.add(idPackage);
                
            }
            
            //EXTRACT THE SUBSCRIPTION PERIOD
            String query3 = "select * from APP.SUBSCRIPTIONS WHERE ID=?";
            PreparedStatement instr3; 
            instr3 = (PreparedStatement) (Statement) con.prepareStatement(query3);
            instr3.setInt(1,idsubscription); 
            ResultSet rs3 = instr3.executeQuery();
            
            if(rs3.next()){
            java.sql.Date period = rs3.getDate("PERIOD2");
            
            
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
"        <link href=\"default.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
"    <div id=\"header\">\n" +
"	<div id=\"logo\">\n" +
"		<h1><a href=\"#\"><span>GYM</span>PUB</a></h1>\n" +
"                <p>Designed by Students for Students</p> \n" +
"	</div>\n" +
"	<div id=\"menu\">\n" +
"		<ul id=\"main\">\n" +
"                    <li class=\"current_page_item\"><a href=\"index.jsp\">Homepage</a></li>\n" +
"                        <li><a href=\"products.jsp\">Subscription</a></li>\n" +
"			<li><a href=\"packageList.jsp\">Packages</a></li>\n" +
"			<li><a href=\"aboutus.jsp\">About Us</a></li>\n" +
"			<li><a href=\"contactus.jsp\">Contact Us</a></li>\n" +
"		</ul>\n" +
"		<ul id=\"feed\">\n" +
"			<li><a href=\"register.jsp\">Register</a></li>\n" +
"			<li><a href=\"login.jsp\">Login</a></li>\n" +
"		</ul>\n" +
"	</div>\n" +
"	\n" +
"</div>\n" +
"        <!-- end header -->\n" +
"    </head>");
            out.println("<body>");
            out.println("<div id=aboutus>");
            out.println("<h2>Subscription Details</h2>");
            out.println("<h4>Your subscription ends on: "+period.toString()+"</h4>");
            out.println("<h2>Packages Details</h2>");
            for(Integer i:listIdPackages){
                String query4 = "select * from APP.PACKAGES WHERE ID=?";
                PreparedStatement instr4; 
                instr4 = (PreparedStatement) (Statement) con.prepareStatement(query4);
                instr4.setInt(1,i); 
                ResultSet rs4 = instr4.executeQuery();
                
                if(rs4.next()){
                out.println("<h4>You are joined to: "+rs4.getString("NAME")+"</h4>");
                }
            }
            
            out.println("<a href=\"index.jsp\">HOME</a>");
            out.println("</id>");
            out.println("</body>");
            out.println("</html>");
            }else{
                
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
"        <link href=\"default.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
"    <div id=\"header\">\n" +
"	<div id=\"logo\">\n" +
"		<h1><a href=\"#\"><span>GYM</span>PUB</a></h1>\n" +
"                <p>Designed by Students for Students</p> \n" +
"	</div>\n" +
"	<div id=\"menu\">\n" +
"		<ul id=\"main\">\n" +
"                    <li class=\"current_page_item\"><a href=\"index.jsp\">Homepage</a></li>\n" +
"                        <li><a href=\"products.jsp\">Subscription</a></li>\n" +
"			<li><a href=\"packageList.jsp\">Packages</a></li>\n" +
"			<li><a href=\"aboutus.jsp\">About Us</a></li>\n" +
"			<li><a href=\"contactus.jsp\">Contact Us</a></li>\n" +
"		</ul>\n" +
"		<ul id=\"feed\">\n" +
"			<li><a href=\"register.jsp\">Register</a></li>\n" +
"			<li><a href=\"login.jsp\">Login</a></li>\n" +
"		</ul>\n" +
"	</div>\n" +
"	\n" +
"</div>\n" +
"        <!-- end header -->\n" +
"    </head>");
            out.println("<body>");
            out.println("<div id=aboutus>");
            out.println("<h2>You do not have subscriptions!</h2>");    
            out.println("<a href=\"products.jsp\">SUBSCRIPTIONS</a>");
            out.println("</id>");    
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(status.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(status.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
