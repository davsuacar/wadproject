/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DAVID
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session=request.getSession();
        String name=request.getParameter("name");
        String sessionName=(String) session.getAttribute("userName");
        boolean isOnDB;
        try {
            //DATABASE CONNECTION
            
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/wadProject","wad","wad");  
            Statement st = con.createStatement();
            String query = "select * FROM APP.USERS";
            ResultSet rs =st.executeQuery(query);
            
            //QUERY TO CHECK THE LOGIN USER
           
            isOnDB = false;
            while(rs.next()){
                String emailDatabase = rs.getString("email");
                if(emailDatabase.equals(name)){
                    isOnDB = true;
                }
            }
            
            
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
"        <link href=\"default.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
"    <div id=\"header\">\n" +
"	<div id=\"logo\">\n" +
"		<h1><a href=\"#\"><span>GYM</span>PUB</a></h1>\n" +
"                <p>Designed by Students for Students</p> \n" +
"	</div>\n" +
"	<div id=\"menu\">\n" +
"		<ul id=\"main\">\n" +
"                    <li class=\"current_page_item\"><a href=\"index.jsp\">Homepage</a></li>\n" +
"                        <li><a href=\"products.jsp\">Subscription</a></li>\n" +
"			<li><a href=\"packageList.jsp\">Packages</a></li>\n" +
"			<li><a href=\"aboutus.jsp\">About Us</a></li>\n" +
"			<li><a href=\"contactus.jsp\">Contact Us</a></li>\n" +
"		</ul>\n" +
"		<ul id=\"feed\">\n" +
"			<li><a href=\"register.jsp\">Register</a></li>\n" +
"			<li><a href=\"login.jsp\">Login</a></li>\n" +
"		</ul>\n" +
"	</div>\n" +
"	\n" +
"</div>\n" +
"        <!-- end header -->\n" +
"    </head>");
            
            out.println("<body>");
            
            
            if(isOnDB == false){
                out.println("<div id=aboutus>");
                out.println("<p>You are not registered!</p>");
                out.println("<a href=\"register.jsp\">Register</a>");
                out.println("</div>");
                session.invalidate();
            }else{
            if(sessionName!=null && name.trim().equals(sessionName))
            out.println("<p>You were already logged in as "+ name+"</p>");
            else 
                if(name.trim().equals("")){
            out.println("<p>Enter a name!</p>");
            session.invalidate();
                }
                else{
            session.setAttribute("userName", name);
            response.sendRedirect("index.jsp");
                }
            out.println("<a href=\"index.jsp\">Back</a>");
            out.println("</body>");
            out.println("</html>");
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
