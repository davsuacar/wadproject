/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.DBConnection;
import model.MyPackage;


/**
 *
 * @author navid
 */
public class PackageBean {
    
    MyPackage pack = new MyPackage();
    
    List<MyPackage> packList = new ArrayList<MyPackage>();

    public List<MyPackage> getPackList() {
        DBConnection dbconnection = new DBConnection();
        Connection con = dbconnection.getConnection();
        String sql="SELECT * FROM APP.PACKAGES";
        
        
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet recordSet = ps.executeQuery();
            while (recordSet.next()) {
                pack = new MyPackage(Integer.parseInt(recordSet.getString(1)),recordSet.getString(2),recordSet.getString(3));
                packList.add(pack);
            }  
            

            ps.close();
            recordSet.close();
        } catch (SQLException ex) {
            System.out.print(ex);
        }
        return packList;
    }

    public void setPackList(List<MyPackage> packList) {
        this.packList = packList;
    }
     
}
