/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author navid
 */
public class MyPackage {
    private int ID;
    private String NAME;
    private String DESCRIPION;

    public MyPackage() {
    }
    
    

    public MyPackage(int ID, String NAME, String DESCRIPION) {
        this.ID = ID;
        this.NAME = NAME;
        this.DESCRIPION = DESCRIPION;
    }

    
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getDESCRIPION() {
        return DESCRIPION;
    }

    public void setDESCRIPION(String DESCRIPION) {
        this.DESCRIPION = DESCRIPION;
    }
    
}
