/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 *
 * @author navid
 */
public class DBConnection {
public Connection con = null;

    public Connection getConnection() 
    {

        try {
            String dbHost = "jdbc:derby://localhost:1527/wadProject";
            String dbUser = "wad";
            String dbPass = "wad";
            con = DriverManager.getConnection(dbHost,dbUser,dbPass);

        } catch (Exception e) {
            System.out.println(e);
        }
        return con;
    }

    public void closeConneciton() {
        try {
            con.close();
        } catch (SQLException ex) {
        }
    }    
}
