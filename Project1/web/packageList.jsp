<%-- 
    Document   : packageList
    Created on : May 19, 2013, 11:36:21 AM
    Author     : navid
--%>
<%@page import="model.MyPackage"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<jsp:useBean id="packageList" class="bean.PackageBean" scope="request" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="default.css" rel="stylesheet" type="text/css" media="screen" />
        <h4 align="right"><% 
    if(session.getAttribute("userName") == null){
    out.println("Guest");
    }else{     
        out.println(session.getAttribute("userName"));
        out.println("|");
        out.println("<a href='status'>Status</a>");
        out.println("|");
        out.println("<a href='logout'>Log out</a>");
        } %></h4>
    <div id="header">
	<div id="logo">
		<h1><a href="#"><span>GYM</span>PUB</a></h1>
                <p>Designed by Students for Students</p> 
	</div>
	<div id="menu">
		<ul id="main">
                    <li class="current_page_item"><a href="index.jsp">Homepage</a></li>
                        <li><a href="products.jsp">Subscription</a></li>
			<li><a href="packageList.jsp">Packages</a></li>
			<li><a href="aboutus.jsp">About Us</a></li>
			<li><a href="contactus.jsp">Contact Us</a></li>
		</ul>
		<ul id="feed">
			<li><a href="register.jsp">Register</a></li>
			<li><a href="login.jsp">Login</a></li>
		</ul>
	</div>
	
</div>
        <!-- end header -->
    </head>
    <body>
        <div id="packageList">
            <form action="addPackage" method="post">
                <%
                    List<MyPackage> packList = new ArrayList<MyPackage>();
                    packList= packageList.getPackList();
                    String select[] =request.getParameterValues("requestPack");
                    if (select != null && select.length != 0) {
                        out.println("You have selected: ");
                        for (int i = 0; i < select.length; i++) {
                             out.println(select[i]); 
                        }
                    }

                    for(int i=0;i<packList.size();++i)
                    { %>
                    <input type="radio" name="requestPack" value="<%= packList.get(i).getID() %>">
                    <label for="username"><%= packList.get(i).getNAME() %></label>
                    <p>
                        <b><%= packList.get(i).getNAME() %></b><%= packList.get(i).getDESCRIPION() %>
                    </p>
                 <%   
                    }
                    
                %>
                <input type="submit" value="Join">
            </form>
        </div>
    </body>
</html>
