<%-- 
    Document   : index
    Created on : 10-may-2013, 18:56:17
    Author     : DAVID
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="default.css" rel="stylesheet" type="text/css" media="screen" />
        <h4 align="right"><% 
    if(session.getAttribute("userName") == null){
    out.println("Guest");
    }else{     
        out.println(session.getAttribute("userName"));
        out.println("|");
        out.println("<a href='status'>Status</a>");
        out.println("|");
        out.println("<a href='logout'>Log out</a>");
        } %></h4>
    <div id="header">
	<div id="logo">
		<h1><a href="#"><span>GYM</span>PUB</a></h1>
                <p>Designed by Students for Students</p> 
	</div>
	<div id="menu">
		<ul id="main">
                    <li class="current_page_item"><a href="index.jsp">Homepage</a></li>
                        <li><a href="products.jsp">Subscription</a></li>
			<li><a href="packageList.jsp">Packages</a></li>
			<li><a href="aboutus.jsp">About Us</a></li>
			<li><a href="contactus.jsp">Contact Us</a></li>
		</ul>
		<ul id="feed">
			<li><a href="register.jsp">Register</a></li>
			<li><a href="login.jsp">Login</a></li>
		</ul>
	</div>
	
</div>
        <!-- end header -->
    </head>
    <body>

        
<div id="wrapper">
	<!-- start page -->
	<div id="page">
        
        <!-- start content -->
		<div id="content">
			<div class="flower"><img src="images/photo-gym.jpg" alt="" width="510" height="250" /></div>
			<div class="post">
				<h1 class="title"><a href="#">Welcome to Our Website!</a></h1>
				<p class="byline"><small>Posted on October 1st, 2014 by <a href="#">Start Reading</a></small></p>
				<div class="entry">
					<p>Fitness First have a number of gyms in London, located all around the London area from the City to North and South.
                                            With the very latest gym equipment and classes on offer, it is the perfect opportunity to use highly trained and enthusiastic personal trainers for that added motivation, a great atmosphere and relax and of course achieve your fitness or weight loss goals as well as making new friends along the way.
                                            To register for a free 1 day gym pass at your nearest Fitness First London gym, visit our free one day gym pass sign up page and complete the form and your chosen club will contact you to arrange your visit.</p>
					<p class="links"><a href="#" class="more">&laquo;&laquo;&nbsp;&nbsp;Read More&nbsp;&nbsp;&raquo;&raquo;</a></p>
				</div>
			</div>
			<div class="post">
				<h2 class="title"><a href="#">New Courses!</a></h2>
				<p class="byline"><small>Posted on October 1st, 2012 by <a href="#">Free CSS Templates</a></small></p>
				<div class="entry">
					<h3>Do not waste time!</h3>
					<blockquote>
						<p>&#8220;Donec leo, vivamus nibh in augue at urna congue rutrum. Quisque dictum integer nisl risus, sagittis convallis, rutrum id, congue, and nibh.&#8221;</p>
					</blockquote>
					<h3>Bulleted List:</h3>
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
						<li>Phasellus nec erat sit amet nibh pellentesque congue.</li>
						<li>Cras vitae metus aliquam risus pellentesque pharetra.</li>
					</ul>
					<h3>Numbered List:</h3>
					<ol>
						<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
						<li>Phasellus nec erat sit amet nibh pellentesque congue.</li>
						<li>Cras vitae metus aliquam risus pellentesque pharetra.</li>
					</ol>
					<p class="links"><a href="#" class="more">&laquo;&laquo;&nbsp;&nbsp;Read More&nbsp;&nbsp;&raquo;&raquo;</a></p>
				</div>
			</div>
        

        
        

        
        
        <!-- +++++++++++++++bottom zone+++++++++++++++ -->
        <div id="cnt_footer">

  <table border="0" cellpadding="5" align="center" cellspacing="0">
  	<tr>
    	<td><a href="#" title="Gimnasio barrio de salamanca">About us</a></td>
        <td>|</td>
    	<td><a href="#" title="Gimansio Madrid">Accesibility</a></td>
        <td>|</td>
    	<td><a href="#" title="Pilates barrio salamanca">Careers</a></td>
        <td>|</td>
    	<td><a href="#" title="Gimnasio serrano">Complaints</a></td>
        <td>|</td>
    	<td><a href="#" title="Gimnasios en barrio de salamanca">Contact us</a></td>
         <td>|</td>
    	<td><a href="#" title="clinicas estetica">FAQs</a></td>
         <td>|</td>
    	<td><a href="#" title="estetica centros">Privacy policy</a></td>
        
        
        
    </tr>
  </table>
  <table width="980" border="0" cellpadding="5" align="center" cellspacing="0">
    <tr>
      <td width="136">&nbsp;</td>
      <td width="680" align="center">Copyright © Regie Fitness | <a href="http://www.regiefitness.com" title="Regie Fitness">www.regiefitness.com</a> - Bucharest</td>
      
    </tr>
  </table>
</div>
    </div>
</div>
</div>
    </body>
</html>
