<%-- 
    Document   : aboutus
    Created on : 14-may-2013, 16:31:23
    Author     : DAVID
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="default.css" rel="stylesheet" type="text/css" media="screen" />
        <h4 align="right"><% 
    if(session.getAttribute("userName") == null){
    out.println("Guest");
    }else{     
        out.println(session.getAttribute("userName"));
        out.println("|");
        out.println("<a href='status'>Status</a>");
        out.println("|");
        out.println("<a href='logout'>Log out</a>");
        } %></h4>
    <div id="header">
	<div id="logo">
		<h1><a href="#"><span>GYM</span>PUB</a></h1>
                <p>Designed by Students for Students</p> 
	</div>
	<div id="menu">
		<ul id="main">
                    <li class="current_page_item"><a href="index.jsp">Homepage</a></li>
                        <li><a href="products.jsp">Subscription</a></li>
			<li><a href="packageList.jsp">Packages</a></li>
			<li><a href="aboutus.jsp">About Us</a></li>
			<li><a href="contactus.jsp">Contact Us</a></li>
		</ul>
		<ul id="feed">
			<li><a href="register.jsp">Register</a></li>
			<li><a href="login.jsp">Login</a></li>
		</ul>
	</div>
	
</div>
        <!-- end header -->
    </head>
    <body>
        <div id="aboutus">
            <h1>ABOUT US</h1>
            <p> 
Our mission at NRG Health & Fitness is to provide top quality equipment and facilities, excellent training and a friendly, welcoming club atmosphere to all our members.
We strive to stay at the leading edge of fitness technology and to generate self-motivation, positive lifestyle changes and a desire to increase health and wellbeing for ever person who comes through our doors, regardless of age, gender or body type.
Our goal is to generate self-motivation, lasting lifestyle changes, and enrich the quality of peoples’ lives through health and fitness.
We want each and every member to enjoy coming to our clubs; to get the most out of every visit, and to benefit from the increased energy that comes with regular exercise.</p>
        </div>
    </body>
</html>
